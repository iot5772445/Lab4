# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np


#TODO 1: modify the following parameters
#Starting and end index, modify this
device_st = 0
device_end = 5

#Path to the dataset, modify this
data_path = "data2/vehicle{}.csv"

#Path to your certificates, modify this
certificate_formatter = "./certificates/device_{}.certificate.pem"
key_formatter = "./certificates/device_{}.private.key"


class MQTTClient:
    def __init__(self, device_id, cert, key, data):
        # For certificate based connection
        self.device_id = str(device_id)
        self.state = 0
        self.data = data
        self.client = AWSIoTMQTTClient(self.device_id)
        #TODO 2: modify your broker address
        self.client.configureEndpoint("a36uz0n2h0bb3s-ats.iot.us-east-2.amazonaws.com", 8883)
        self.client.configureCredentials("AmazonRootCA1.pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        self.client.subscribeAsync(f"emissions/max/{self.device_id}", 0, ackCallback=self.customSubackCallback)

    def customOnMessage(self,message):
        #TODO3: fill in the function to show your received message
        print("\nvehicle {} received max co2 reading {} from topic {}".format(self.device_id, json.loads(message.payload)['max'], message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        #You don't need to write anything here
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        #You don't need to write anything here
        pass


    def publish(self):
        sending_dict = self.data.iloc[self.state].to_dict()
        sending_dict['sequence_num'] = self.state
        
        self.client.publishAsync(f"emissions/readings/{self.device_id}", json.dumps(sending_dict), 0, ackCallback=self.customPubackCallback)
        self.state += 1


print("Initializing MQTTClients...")
clients = []
for device_id in range(device_st, device_end):
    device_data = pd.read_csv(data_path.format(device_id))
    device_data['vehicle_id'] = str(device_id)
    device_data['highest_sequence_num'] = str(len(device_data) - 1)
    client = MQTTClient(device_id,certificate_formatter.format(device_id) ,key_formatter.format(device_id), device_data)
    client.client.connect()
    clients.append(client)
 
counter = 0
while True:
    # print("send now?")
    x = 's'#input()
    if x == "s":
        onesuccess = False
        for i,c in enumerate(clients):
            try:
                c.publish()
                if c.device_id == '4':
                    print(f'{c.state}/{c.data.shape[0]}')
                onesuccess = True
            except Exception as e:
                # print(e)
                1
        # print(f'\r{counter}',end='')
        if not onesuccess:
            break
        counter += 1
    elif x == "d":
        for c in clients:
            c.client.disconnect()
        print("All devices disconnected")
        exit()
    else:
        print("wrong key pressed")

    time.sleep(0.1)

input('Done. Keeping alive for reciept')




