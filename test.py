################################################### Connecting to AWS
import boto3

import json

def createThing(thingClient, device_number):
    thingName = f'thing_{device_number}'
    thingResponse = thingClient.create_thing(
        thingName = thingName
    )
    data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
    thingArn = data['thingArn']
    thingId = data['thingId']
    createCertificate(thingClient, thingName, device_number)
    return thingArn, thingId

def createCertificate(thingClient, thingName, device_number):
    certResponse = thingClient.create_keys_and_certificate(
            setAsActive = True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data: 
            if element == 'certificateArn':
                    certificateArn = data['certificateArn']
            elif element == 'keyPair':
                    PublicKey = data['keyPair']['PublicKey']
                    PrivateKey = data['keyPair']['PrivateKey']
            elif element == 'certificatePem':
                    certificatePem = data['certificatePem']
            elif element == 'certificateId':
                    certificateId = data['certificateId']
                            
    with open(f'certificates/device_{device_number}.public.key', 'w') as outfile:
            outfile.write(PublicKey)
    with open(f'certificates/device_{device_number}.private.key', 'w') as outfile:
            outfile.write(PrivateKey)
    with open(f'certificates/device_{device_number}.certificate.pem', 'w') as outfile:
            outfile.write(certificatePem)

    response = thingClient.attach_policy(
            policyName = defaultPolicyName,
            target = certificateArn
    )
    response = thingClient.attach_thing_principal(
            thingName = thingName,
            principal = certificateArn
    )
    response = thingClient.add_thing_to_thing_group(
            thingName = thingName,
            thingGroupName = defaultGroupName
    )

thingClient = boto3.client('iot')
################################################### Parameters for Thing
defaultPolicyName = 'thing_1_policy'
defaultGroupName = 'thing_group_1'
###################################################
for i in range(5):
    thingArn, thingId = createThing(thingClient, i)